﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;
    [SerializeField] private bool isPlayableQuest; //activate UI only if it is a time based quest
    public static Action QuestFinished;
    

    void Start()
    {
        QuestFinished += OnQuestFinished;
        if (isPlayableQuest)
        {
            uiManager.Initialize();
            GameState(GameStates.Gameplay);
        }
    }

    private void GameState(GameStates gameState)
    {
        switch (gameState)
        {
            case GameStates.Tutorial:
                break;

            case GameStates.Gameplay:
                if (UIManager.ChangeView != null)
                {
                    UIManager.ChangeView(UIView.HUD, true);
                }
                break;

            default:
                break;
        }
    }

    //Load a new Scene based on the current quest state
    private void OnQuestFinished()
    {
        switch (GlobalObjects.QuestState)
        {
            case QuestState.PPobladoPresent1:
                SceneManager.LoadScene(Scenes.Tutorial.ToString());
                GlobalObjects.QuestState = QuestState.Tutorial;
            break;

            case QuestState.Tutorial:
                SceneManager.LoadScene(Scenes.PPobladoPast.ToString());
                GlobalObjects.QuestState = QuestState.PPobladoPast;
            break;

            case QuestState.PPobladoPast:
                SceneManager.LoadScene(Scenes.SantaFe.ToString());
                GlobalObjects.QuestState = QuestState.SantaFe;
            break;

            case QuestState.SantaFe:
                SceneManager.LoadScene(Scenes.PPobladoPresent.ToString());
                GlobalObjects.QuestState = QuestState.PPobladoPresent2;
            break;

            case QuestState.PPobladoPresent2:
                SceneManager.LoadScene(Scenes.PBerrioPresent.ToString());
                GlobalObjects.QuestState = QuestState.PBerrioPresent1;
            break;

            case QuestState.PBerrioPresent1:
                SceneManager.LoadScene(Scenes.PBerrioPast.ToString());
                GlobalObjects.QuestState = QuestState.PBerrioPast;
            break;

            case QuestState.PBerrioPast:
                SceneManager.LoadScene(Scenes.PBerrioPresent.ToString());
                GlobalObjects.QuestState = QuestState.PBerrioPresent2;
                QuestFinished -= OnQuestFinished;
            break;
            
            default:
            break;
        }
    }
}
