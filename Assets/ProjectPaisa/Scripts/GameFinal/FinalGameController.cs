﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FinalGameController : MonoBehaviour
{
    [SerializeField] private GameObject buttonColumn;
    [SerializeField] private GameObject CharactersDancing;
    [SerializeField] private GameObject PlayerImage;
    [SerializeField] private AudioSource music;
    public static UnityAction startFinalBehaviour;

    private void Awake()
    {
        startFinalBehaviour += OnStartFinalBehaviour;
        CharactersDancing.SetActive(false);
        PlayerImage.SetActive(false);
    }
    private void Start()
    {
        SoundManager.PlaySound(Dialogues.Final.ToString(), false);
    }

    private void OnStartFinalBehaviour()
    {
        music.Play();
        SoundManager.PlaySound(Dialogues.Final_1.ToString(), false);
        CharactersDancing.SetActive(true);
        buttonColumn.SetActive(false);
        PlayerImage.SetActive(true);
    }

    private void OnDisable()
    {
        startFinalBehaviour -= OnStartFinalBehaviour;
    }
}
