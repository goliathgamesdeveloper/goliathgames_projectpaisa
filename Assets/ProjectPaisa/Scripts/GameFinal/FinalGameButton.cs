﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalGameButton : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        FinalGameController.startFinalBehaviour();
    }
}
