﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_LetterBoxSensor : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.tag == "PaperTreasure")
        {
            MisionController.CompleteAllQuestActivity();
            UIManager.ChangeView(UIView.QuestGameOverWin, false);
        }
    }
}
