﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Quest1_Arrieros1Behaviour : MonoBehaviour
{
    public static Action<Quest1_Arreieros1_ItemType, bool> ItemInContainer;
    private Vector3[] initItemsPositions;
    private Vector3[] initItemsRotation;
    private Vector3[] initArrierosPositions;
    private Vector3[] initArrierosRotations;
    [SerializeField] private Transform[] items;
    [SerializeField] private TimerArcade timerArcade;
    [SerializeField] private GameObject mule;
    [SerializeField] private GameObject letterBox;
    [SerializeField] private ArrieroBehaviour arriero;
    [SerializeField] private GameObject[] arrieros;
    [SerializeField] private AudioSource music;
    private Vector3 muleStartPosition;
    private int muleWalkDuration = 10;
    private float muleCurrentDuration = 0;
    private float muleWalkDelay = 0.1f;
    private int itemsInContainer = 0;


    private void Awake()
    {
        letterBox.SetActive(false);
        ItemInContainer += OnItemInContainer;
        muleStartPosition = mule.transform.position;
        InitArrierosPosition();
        InitItemsPosition();
    }

    private void OnDisable()
    {
        ItemInContainer -= OnItemInContainer;
    }

    private void Start()
    {
        SoundManager.PlaySound(Dialogues.Mision_2.ToString(), false);
        music.Play();
    }

    private void OnItemInContainer(Quest1_Arreieros1_ItemType itemType, bool success)
    {

        Quest1_Arrieros1_ArrieroBehaviour arriero = SearchArriero(itemType);
        if (success)
        {
            itemsInContainer += 1;
            arriero.GoToHome();
            arriero.DoAnimation(Quest1_Arrieros1_CharactersAnimation.No, false);
            arriero.DoAnimation(Quest1_Arrieros1_CharactersAnimation.Run, true);

            if (CheckAllItems())
            {
                //complete, call to mision controller and call all activitys completed, and uimanager for continue view
                timerArcade.StopAllCoroutines();
                StartCoroutine(DoMuleWalkOverTime());
                letterBox.SetActive(true);
                SoundManager.PlaySound(Dialogues.Pergamine.ToString(), false);
            }
        }
        else
        {
            arriero.DoAnimation(Quest1_Arrieros1_CharactersAnimation.No, true);
        }
        
    }

    private Quest1_Arrieros1_ArrieroBehaviour SearchArriero(Quest1_Arreieros1_ItemType itemType)
    {
        
        for (int i = 0; i < arrieros.Length; i++)
        {
            Quest1_Arreieros1_ItemType item = arrieros[i].transform.GetComponent<Quest1_Arrieros1_ItemType>().ItemTypeSelected;
            if (item == itemType)
            {
                return arrieros[i].GetComponent<Quest1_Arrieros1_ArrieroBehaviour>();
            }
        }
        return null;
    }

    private bool CheckAllItems()
    {
        if (itemsInContainer == items.Length)
        {
            return true;
        }
        return false;
    }

    private void InitArrierosPosition()
    {
        initArrierosPositions = new Vector3[arrieros.Length];
        initArrierosRotations = new Vector3[arrieros.Length];

        for (int i = 0; i < arrieros.Length; i++)
        {
            initArrierosPositions[i] = arrieros[i].transform.position;
            initArrierosRotations[i] = arrieros[i].transform.eulerAngles;
        }
    }

    private void InitItemsPosition()
    {
        initItemsPositions = new Vector3[items.Length];
        initItemsRotation = new Vector3[items.Length];
        
        for (int i = 0; i < items.Length; i++)
        {
            initItemsPositions[i] = items[i].transform.position;
            initItemsRotation[i] = items[i].transform.eulerAngles;
        }
    }

    public void StopAllActivitys()
    {
        timerArcade.StopAllCoroutines();

        for (int i = 0; i < arrieros.Length; i++)
        {
            arrieros[i].transform.GetComponent<Quest1_Arrieros1_ArrieroBehaviour>().StopAllCoroutines();
        }
    }

    public void ResetActivity()
    {
        music.Play();
        itemsInContainer = 0;
        muleCurrentDuration = 0;
        mule.transform.position = muleStartPosition;
        StopAllCoroutines();
        StopCoroutine(DoMuleWalkOverTime());
        letterBox.SetActive(false);
        ResetItems();
        ResetArrieros();
        ResetArrierosAnimations();
        ResetItemsData();
        timerArcade.StartTimer();
        arriero.DoAnimation(ArrieroAnimations.Salute, true);
        arriero.DoAnimation(ArrieroAnimations.Defeat, false);
        arriero.DoAnimation(ArrieroAnimations.Victory, false);
    }

    private void ResetArrierosAnimations()
    {
        for (int i = 0; i < arrieros.Length; i++)
        {
            arrieros[i].GetComponent<Quest1_Arrieros1_ArrieroBehaviour>().DoAnimation(Quest1_Arrieros1_CharactersAnimation.Run, false);
            arrieros[i].GetComponent<Quest1_Arrieros1_ArrieroBehaviour>().DoAnimation(Quest1_Arrieros1_CharactersAnimation.No, false);
        }
    }

    private void ResetItems()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].transform.gameObject.SetActive(true);
            items[i].transform.position = initItemsPositions[i];
            Quaternion itemRotation = Quaternion.Euler(initItemsRotation[i]);
            items[i].transform.rotation = itemRotation;
        }
    }

    private void ResetArrieros()
    {
        for (int i = 0; i < arrieros.Length; i++)
        {
            arrieros[i].GetComponent<Quest1_Arrieros1_ArrieroBehaviour>().ResetArrieroBehaviour();
            arrieros[i].transform.GetComponent<Quest1_Arrieros1_ArrieroBehaviour>().StopAllCoroutines();
            arrieros[i].transform.position = initArrierosPositions[i];
            Quaternion itemRotation = Quaternion.Euler(initArrierosRotations[i]);
            arrieros[i].transform.rotation = itemRotation;
        }
    }

    public void DoGameOverBehaviour()
    {
        music.Stop();
        HideItems();
        arriero.DoAnimation(ArrieroAnimations.Defeat, true);
        StartCoroutine(DoMuleWalkOverTime());
        SoundManager.PlaySound(Dialogues.Audio_Defeat.ToString(), false);
        SoundManager.PlaySound(SFX.SFX_Defeat.ToString(), false);
    }

    public void DoGameOverWinBehaviour()
    {
        arriero.DoAnimation(ArrieroAnimations.Victory, true);
        SoundManager.PlaySound(Dialogues.Audio_Victory.ToString(), false);
        SoundManager.PlaySound(SFX.SFX_Victory.ToString(), false);
    }

    private void ResetItemsData()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].GetComponent<Quest1_Arrieros1_ItemType>().ItemCompleted = false;
            items[i].GetComponent<GrabBehaviour>().isItemUsedInQuest = false;
        }
    }

    private IEnumerator DoMuleWalkOverTime()
    {
        while (muleCurrentDuration <= muleWalkDuration)
        {
            mule.transform.Translate(Vector3.forward * muleWalkDelay, Space.Self);
            muleCurrentDuration += muleWalkDelay;
            
            yield return new WaitForSeconds(muleWalkDelay);
        }
        StopCoroutine(DoMuleWalkOverTime());
    }

    private void HideItems()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].gameObject.SetActive(false);
        }
    }


}
