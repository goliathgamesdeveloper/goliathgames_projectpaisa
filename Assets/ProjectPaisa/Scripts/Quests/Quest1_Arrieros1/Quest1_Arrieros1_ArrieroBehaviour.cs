﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Quest1_Arrieros1_ArrieroBehaviour : MonoBehaviour
{
    private int arrieroWalkDuration = 25;
    private float arrieroCurrentDuration = 0;
    private float arrieroCurrentRotation = 0;
    private float arrieroWalkDelay = 0.1f;
    private Canvas itemDescription;
    private Animator animator;
    private bool canMove = false;
    private UnityAction startWalk; 

    private void Awake()
    {
        itemDescription = GetComponentInChildren<Canvas>();
        animator = GetComponent<Animator>();
    }

    public void ResetArrieroBehaviour()
    {
        arrieroCurrentDuration = 0;
        arrieroCurrentRotation = 0;
        StopCoroutine(DoArrieroRotateOverTime());
        StopCoroutine(DoArrieroWalkOverTime());
        itemDescription.gameObject.SetActive(true);
    }

    public void GoToHome()
    {
        startWalk += OnStartWalk;
        Debug.Log("me voy a casa!!!");
        StartCoroutine(DoArrieroRotateOverTime());
        itemDescription.gameObject.SetActive(false);
    }

    public void DoAnimation(Quest1_Arrieros1_CharactersAnimation animation, bool state)
    {
        animator.SetBool(animation.ToString(), state);
    }

    private void OnStartWalk()
    {
        StartCoroutine(DoArrieroWalkOverTime());
    }

    private IEnumerator DoArrieroRotateOverTime()
    {
        while (arrieroCurrentRotation <= 180)
        {
            transform.Rotate(Vector3.up * 5, Space.Self);
            arrieroCurrentRotation += 5f;
            yield return new WaitForSeconds(0.1f); 
        }
        StopCoroutine(DoArrieroRotateOverTime());
        startWalk();
    }

    private IEnumerator DoArrieroWalkOverTime()
    {
        while (arrieroCurrentDuration <= arrieroWalkDuration)
        {
            transform.Translate(Vector3.forward * arrieroWalkDelay, Space.Self);
            arrieroCurrentDuration += arrieroWalkDelay;
            yield return new WaitForSeconds(arrieroWalkDelay);
        }
        startWalk -= OnStartWalk;
        StopCoroutine(DoArrieroWalkOverTime());
        
    }

    private void OnDisable()
    {
        startWalk -= OnStartWalk;
    }
}
