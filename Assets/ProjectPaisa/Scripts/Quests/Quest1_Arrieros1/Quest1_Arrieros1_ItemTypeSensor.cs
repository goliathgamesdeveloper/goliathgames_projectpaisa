﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_ItemTypeSensor : MonoBehaviour
{
    private Quest1_Arreieros1_ItemType itemType;
    private bool itemCompleted;
    private Quest1_Arrieros1_ItemType item;
    [SerializeField] private Quest1_Arreieros1_ItemType itemTypeSelected;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Quest1_Items")
        {
            item = collision.gameObject.GetComponent<Quest1_Arrieros1_ItemType>();
            itemType = item.ItemTypeSelected;

            if (itemType == itemTypeSelected && !item.ItemCompleted)
            {
                item.ItemCompleted = true;
                item.HideItem();
                Quest1_Arrieros1Behaviour.ItemInContainer(itemType, true);
            } else if (itemType != itemTypeSelected)
            {
                item.GetComponent<ItemFloorSensor>().ResetItemPosition();
                Quest1_Arrieros1Behaviour.ItemInContainer(itemTypeSelected, false);
            }          
        }
    }
}
