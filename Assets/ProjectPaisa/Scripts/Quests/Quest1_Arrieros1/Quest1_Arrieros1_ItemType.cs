﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_ItemType : MonoBehaviour
{
    [SerializeField] private Quest1_Arreieros1_ItemType itemTypeSelected;
    public Quest1_Arreieros1_ItemType ItemTypeSelected
    {
        get { return itemTypeSelected; }
    }
    private bool itemCompleted = false;
    public bool ItemCompleted
    {
        get { return itemCompleted; }
        set { itemCompleted = value; }
    }

    public void HideItem()
    {
        StartCoroutine(WaitToHide());
    }

    private IEnumerator WaitToHide()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
}
