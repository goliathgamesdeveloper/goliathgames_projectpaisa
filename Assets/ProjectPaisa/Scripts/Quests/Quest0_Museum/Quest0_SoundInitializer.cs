﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest0_SoundInitializer : MonoBehaviour
{
    void Start()
    {
        string[] stackDialogues = new string[]
        {
            Dialogues.Audio_Quest0_Nota1.ToString(),
            Dialogues.Audio_Quest0_Nota2.ToString(),
            Dialogues.Audio_Quest0_Nota3.ToString()
        };

        if (SoundManager.PlaySound != null)
        {
            SoundManager.PlaySoundStack(stackDialogues);
        }
    }

}
