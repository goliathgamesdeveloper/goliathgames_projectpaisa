﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButton : ButtonBaseBehaviour
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnTriggerEnter(Collider collision)
    {
        base.OnTriggerEnter(collision);
        TutorialManager.tutorialState(TutorialState.Grab);
    }

}
