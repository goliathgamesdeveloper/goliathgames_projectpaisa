﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class TutorialManager : MonoBehaviour
{
    public static Action<TutorialState> tutorialState;
    [SerializeField] private GameObject button;
    [SerializeField] private GameObject itemGrab;
    [SerializeField] private VideoPlayer videoPlayer;

    private float currentDuration = 0;

    private void Awake()
    {
        button.SetActive(false);
        itemGrab.SetActive(false);
        tutorialState += OnTutorialState;
    }
    private void Start()
    {
        OnTutorialState(TutorialState.Start);
    }

    private void OnTutorialState(TutorialState state)
    {
        switch (state)
        {
            case TutorialState.Start:
                OnTutorialState(TutorialState.Button);
                
                break;
            case TutorialState.Button:
                SoundManager.PlaySound(Dialogues.Tutorial.ToString(), false);
                videoPlayer.clip = Resources.Load<VideoClip>("Videos/ButtonPress") as VideoClip;
                videoPlayer.Play();
                button.SetActive(true);
                break;
            case TutorialState.Grab:
                button.SetActive(false);
                itemGrab.SetActive(true);
                SoundManager.PlaySound(Dialogues.Tutorial_1.ToString(), false);
                videoPlayer.clip = Resources.Load<VideoClip>("Videos/Grip") as VideoClip;
                videoPlayer.Play();
                break;
            case TutorialState.Completed:
                button.SetActive(false);
                SoundManager.PlaySound(Dialogues.Tutorial_2.ToString(), false);
                StartCoroutine(WaitForCompleteTutorial());
                break;
            default:
                break;
        }
    }

    private IEnumerator WaitForCompleteTutorial()
    {
        while (currentDuration <= 9)
        {
            currentDuration += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        SceneManager.LoadScene(Scenes.Quest1_Arrieros.ToString());
        StopCoroutine(WaitForCompleteTutorial());
    }

    private void OnDisable()
    {
        tutorialState -= OnTutorialState;
    }
}
