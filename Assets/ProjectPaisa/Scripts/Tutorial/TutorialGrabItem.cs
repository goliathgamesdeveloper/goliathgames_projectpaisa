﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGrabItem : OVRGrabbable
{
    private GameObject itemDescription;

    protected override void Awake()
    {
        base.Awake();
        if (GetComponentInChildren<Canvas>() != null)
        {
            itemDescription = GetComponentInChildren<Canvas>().gameObject;
            itemDescription.SetActive(false);
        }
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    {
        base.GrabBegin(hand, grabPoint);
        if (itemDescription != null)
            itemDescription.SetActive(true);

        TutorialManager.tutorialState(TutorialState.Completed);
    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        base.GrabEnd(linearVelocity, angularVelocity);
        if (itemDescription != null)
            itemDescription.SetActive(false);
    }
}
