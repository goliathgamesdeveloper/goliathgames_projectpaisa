﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Quest0ButtonStartGameplay : ButtonBaseBehaviour
{

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            base.OnTriggerEnter(collision);
            MisionController.CompleteQuest(true);
        }   
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
