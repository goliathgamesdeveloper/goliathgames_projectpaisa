﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGameOverBtnRestart : ButtonBaseBehaviour
{
    protected override void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.gameObject.tag == "Player")
        {
            base.OnTriggerEnter(collision);
            MisionController.RestartQuest();
            transform.parent.parent.gameObject.SetActive(false);
        }   
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
