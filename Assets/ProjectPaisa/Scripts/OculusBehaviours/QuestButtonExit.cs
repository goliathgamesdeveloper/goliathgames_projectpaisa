﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestButtonExit : ButtonBaseBehaviour
{
    protected override void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            base.OnTriggerEnter(collision);
            Application.Quit();
        }   
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
