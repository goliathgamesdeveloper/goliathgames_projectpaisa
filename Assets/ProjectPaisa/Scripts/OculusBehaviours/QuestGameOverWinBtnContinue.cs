﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGameOverWinBtnContinue : ButtonBaseBehaviour
{
    protected override void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            base.OnTriggerEnter(collision);
            MisionController.CompleteQuest(true);
            transform.parent.parent.gameObject.SetActive(false);
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
