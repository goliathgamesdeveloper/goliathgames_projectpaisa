﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ButtonBaseBehaviour : MonoBehaviour
{
    Animator animator;

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
    }

    protected virtual void OnTriggerEnter(Collider collision)
    {
        animator.SetBool("Pusher", true);
    }

    protected virtual void OnDisable()
    {
        animator.SetBool("Pusher", false);
    }
}
