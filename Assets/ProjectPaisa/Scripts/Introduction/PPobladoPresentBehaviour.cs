﻿using System.Collections;
using UnityEngine;

public class IntroductionBehaviour : MonoBehaviour
{
    private float currentDuration = 0;
    private void Awake()
    {
        SoundManager.PlaySound(Dialogues.Intro.ToString(), false);
        
        StartCoroutine(WaitForCompleteIntroduction());
    }

    private IEnumerator WaitForCompleteIntroduction()
    {
        while (currentDuration <= 26)
        {
            currentDuration += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        if (GameManager.QuestFinished != null){
            StopCoroutine(WaitForCompleteIntroduction());
            GameManager.QuestFinished();
        }
    }
}
