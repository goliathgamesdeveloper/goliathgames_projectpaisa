﻿using System.Collections;
using UnityEngine;

public class PPobladoPresentBehaviour : MonoBehaviour
{
    private float currentDuration = 0;
    [SerializeField] private MainGuideBehaviour mainGuideBehaviour;
    private Coroutine audioCoroutine;

    private void Awake()
    { 
        audioCoroutine = StartCoroutine(mainGuideBehaviour.PlayDialogue(1, OnCompleteDialogue)); 
    }

    public void OnCompleteDialogue()
    {
        if (GameManager.QuestFinished != null){
            StopCoroutine(audioCoroutine);
            GameManager.QuestFinished();
        }
    }
}
