﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFloorSensor : MonoBehaviour
{
    private Vector3 itemPosition;

    private void Awake()
    {
        itemPosition = transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            transform.position = itemPosition;
        }     
    }

    public void ResetItemPosition()
    {
        StartCoroutine(WaitToReset());
    }

    private IEnumerator WaitToReset()
    {
        yield return new WaitForSeconds(1f);
        transform.position = itemPosition;
    }
}
