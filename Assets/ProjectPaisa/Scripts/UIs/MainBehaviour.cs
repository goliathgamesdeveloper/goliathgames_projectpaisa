﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainBehaviour : UIBase
{
    public override void Initialize(Camera camera)
    {
        base.Initialize(camera);
    }

    public void GoToGameplay()
    {
        SceneManager.LoadScene(Scenes.Quest0_Inicio.ToString());
    }

    public void Quit()
    {
        Application.Quit();
    }
}
