﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerArcade : MonoBehaviour
{
    private float startingTime;
    private float delay = .1f;
    private float loseTime = 0f;
    private TextMeshPro time_Txt;

    public void Initialise(TextMeshPro text)
    {
        time_Txt = text;
    }

    public void StartTimer()
    {
        startingTime = 160f;
        StartCoroutine(TimerCountDown());
    }


    public void UpdateTimer(int time)
    {
        startingTime += time;
    }

    private IEnumerator TimerCountDown()
    {
        while (startingTime > loseTime)
        {
            string minutes = Mathf.Floor(startingTime / 60).ToString("00");
            string seconds = (startingTime % 60).ToString("00");
           
            time_Txt.text = "Tiempo: " + minutes + ":" + seconds;
            startingTime -= delay;
            yield return new WaitForSeconds(delay);
        }
        StopCoroutine(TimerCountDown());
        MisionController.CompleteQuest(false);
    }
}
