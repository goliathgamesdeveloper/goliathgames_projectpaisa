﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBase : MonoBehaviour
{
    protected UIView view;
    public UIView View
    {
        get {return view;}
    }

    protected Canvas canvas;
    protected CanvasScaler canvasScaler;

    public virtual void Initialize(Camera camera)
    {
        canvas = GetComponent<Canvas>();
        canvasScaler = GetComponent<CanvasScaler>();

        //Renders
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = camera;
        canvas.planeDistance = 1;

        //Scalers
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);
    }

    protected virtual void Awake()
    {
        
    }

    public virtual void CloseView(bool isPrimary)
    {
        if (isPrimary)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
}
