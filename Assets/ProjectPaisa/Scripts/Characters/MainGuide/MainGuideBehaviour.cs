﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainGuideBehaviour : MonoBehaviour
{
    private AudioSource audioSource;
    private AudioClip dialogueAudioClip;
    private string audioClipPath = "Sounds/Dialogues/MainGuide/Voice_Don_Julian_";
    WaitForSeconds whileAudioClipWaitTime = new WaitForSeconds(.1f); //Time to wait while the audio is playing, so it don't go out execution
    WaitForSeconds audioClipFinishedWaitTime = new WaitForSeconds(.5f); //Time to wait when the audio finish playing

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public IEnumerator PlayDialogue(int numberOfDialogue, Action completeDialogue)
    {
        dialogueAudioClip = Resources.Load<AudioClip>(audioClipPath + numberOfDialogue);
        Debug.Log(dialogueAudioClip + "path");
        audioSource.clip = dialogueAudioClip;
        audioSource.Play();

        while (audioSource.isPlaying)
        {
            yield return whileAudioClipWaitTime;
        }

        if (!audioSource.isPlaying) // the audio clip finished
        {
            yield return audioClipFinishedWaitTime;
            if (completeDialogue != null)
                completeDialogue();
        }
    }
}
