﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrieroBehaviour : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void DoAnimation(ArrieroAnimations animation, bool state)
    {
        animator.SetBool(animation.ToString(), state);
    }
}
