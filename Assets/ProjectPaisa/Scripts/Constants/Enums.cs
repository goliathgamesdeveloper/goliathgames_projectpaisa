﻿public enum Quests
{
    Quest0_Inicio = 0,
    Quest1_Arrieros = 1,
    Quest1_Arrieros1 = 2,
    Museum = 3
}

public enum Scenes
{
    Quest0_Inicio = 0,
    Quest1_Arrieros = 1,
    Quest1_Arrieros1 = 2,
    Main = 4,
    Introduction = 5,
    Tutorial = 6,
    PPobladoPresent = 7,
    PPobladoPast = 8,
    SantaFe = 9,
    PBerrioPresent = 10,
    PBerrioPast = 11
}

public enum QuestState
{
    PPobladoPresent1 = 0,
    Tutorial = 1,
    PPobladoPast = 2,
    SantaFe = 3,
    PPobladoPresent2 = 4,
    PBerrioPresent1 = 5,
    PBerrioPast = 6,
    PBerrioPresent2 = 7
}

public enum UIView
{
    None = -1,
    Main = 0,
    HUD = 1,
    Settings = 2,
    Loading = 3,
    QuestGameOver = 4,
    QuestGameOverWin = 5
}

public enum GameStates
{
    Gameplay = 0,
    Pause = 1,
    Tutorial = 2,
    GameOver = 3,
    Main = 4
}

public enum ArrieroAnimations
{
    Salute = 0,
    Victory = 1,
    Defeat = 2
}

public enum Quest1_Arrieros1_CharactersAnimation
{
    Run = 0,
    No = 1
}

public enum Quest1_Arreieros1_ItemType
{
    Wood = 0,
    Gold = 1,
    Coffe = 2,
    Cloth = 3
    
}

public enum Dialogues
{
    Audio_Creditos = 0,
    Audio_Defeat = 1,
    Audio_Quest0_Nota1 = 2,
    Audio_Quest0_Nota2 = 3,
    Audio_Quest0_Nota3 = 4,
    Audio_Quest1_1_Nota2 = 5,
    Audio_Quest1_1_Nota3  = 6,
    Audio_Quest1_Nota1 = 7,
    Audio_Victory = 8,
    Intro = 9,
    Tutorial = 10,
    Tutorial_1 = 11,
    Tutorial_2 = 12,
    Mision_1 = 13,
    Mision_2 = 14,
    Pergamine = 15,
    Final = 16,
    Final_1 = 17,
}

public enum SFX
{
    SFX_Defeat = 0,
    SFX_Victory = 1,
    CitySound = 2,
}

public enum TutorialState
{
    Start = 0,
    Button = 1,
    Grab = 2,
    Completed = 3
}